package infrastructure;

import java.util.HashMap;

import domain.Aggregate.ReservationVoyagesPourUneModalite;
import domainRepository.Repository;

public class VoyageRepository implements Repository {

	
	public HashMap<Integer,ReservationVoyagesPourUneModalite> voyages = new HashMap<Integer,ReservationVoyagesPourUneModalite>();
	

	public ReservationVoyagesPourUneModalite findById(int id) {
		return voyages.get(id);
		
	}

	public void save(ReservationVoyagesPourUneModalite voyage) {
		voyages.put(voyage.getId(),voyage);
	}
	
	

}
