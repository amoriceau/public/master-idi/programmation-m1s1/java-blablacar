package application;

import java.util.Date;
import java.util.List;

import domain.Aggregate.ReservationVoyagesPourUneModalite;
import domain.Vo.Trajet;
import domain.Vo.TypeModalite;
import domainRepository.Repository;
import infrastructure.VoyageRepository;

public class ReservationVoyagesPourUneModaliteServices {
	
	private final Repository repo = new VoyageRepository();
	private static int cpt = 0;

	public int creerAggregate(TypeModalite t) {
		ReservationVoyagesPourUneModalite r = new ReservationVoyagesPourUneModalite(cpt++,t);
		repo.save(r);
		return r.getId();
	}

	public void addTrajet(int idagg, Trajet t1) {
		ReservationVoyagesPourUneModalite r = repo.findById(idagg);
		r.addTrajet(t1);
		repo.save(r);
	}

	public List<Trajet> getListTrajets(int idagg) {
		return repo.findById(idagg).getListTrajets();
	}

	public boolean reserverUneplaceDansVoyage(int idagg, Integer idvoyage) {

		try {
			ReservationVoyagesPourUneModalite r = repo.findById(idagg);
			r.reserverUneplaceDansVoyage(idvoyage);
			repo.save(r);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public int addVoyage(int idagg, Trajet t1, Date date, int i) {
		ReservationVoyagesPourUneModalite r = repo.findById(idagg);
		int j = r.addVoyage(t1, date, i);
		repo.save(r);
		return j;
	}

	public List<Integer> getListVoyages(int idagg) {
		ReservationVoyagesPourUneModalite r = repo.findById(idagg);
		return r.getListVoyages();
	}

	public String getAsStringVoyage(int idagg, int id) {
		ReservationVoyagesPourUneModalite r = repo.findById(idagg);
		return r.getAsStringVoyage(id);
	}

	public List<Integer> getListVoyages(int idagg, Date date, Date date2) {
		ReservationVoyagesPourUneModalite r = repo.findById(idagg);
		List<Integer> ids = r.getListVoyages(date,date2);
		return ids;
	}

}
