package domain.Aggregate;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import domain.Entite.Voyage;
import domain.Vo.Trajet;
import domain.Vo.TypeModalite;

public class ReservationVoyagesPourUneModalite {

	private List<Trajet> trajets;
	private List<Voyage> voyages;
	private static int cpt = 0;
	private int id;
	private TypeModalite modalite;
	// type transport

	public ReservationVoyagesPourUneModalite(int id, TypeModalite t) {
		this.id = id;
		trajets = new LinkedList<>();
		voyages = new LinkedList<>();
		this.modalite = t;
	}

	public TypeModalite getModalite() {
		return modalite;
	}

	public void addTrajet(Trajet t1) {
		trajets.add(t1);
	}

	public List<Trajet> getListTrajets() {
		List<Trajet> tmp = new LinkedList<>();
		for (Trajet trajet : trajets) {
			tmp.add(trajet);
		}
		return tmp;
	}

	public void reserverUneplaceDansVoyage(Integer id) throws Exception {
		Voyage v = getVoyage(id);
		v.prendreUnSiege();

	}

	public int addVoyage(Trajet t1, Date date, int i) {
		cpt = cpt + 1;
		Voyage v = new Voyage(cpt, t1, date, i);
		voyages.add(v);
		return cpt;
	}

	public List<Integer> getListVoyages() {
		LinkedList<Integer> l = new LinkedList<>();
		for (Voyage v : voyages) {
			l.add(v.getId());
		}
		return l;
	}

	public String getAsStringVoyage(int id) {
		return new String(modalite.toString()+" "+getVoyage(id).toString());
	}

	private Voyage getVoyage(int id) {
		for (Voyage v : voyages) {
			if (v.getId() == id) {
				return v;
			}
		}
		return null;
	}

	public int getId() {

		return id;
	}

	public List<Integer> getListVoyages(Date date, Date date2) {
		List<Integer> tmp = new LinkedList<>();
		for (Voyage v : voyages) {
			if((v.getD().getTime()>=date.getTime())&&(v.getD().getTime()<=date2.getTime())) {
				tmp.add(v.getId());
			}
		}
		return tmp;
	}
}
