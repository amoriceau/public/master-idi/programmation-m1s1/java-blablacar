package domain.Entite;

import java.util.Date;

import domain.Vo.Trajet;

public class Voyage {
	private int id;
	private Trajet t;
	private Date d;
	private int nbPlacesLibres;

	public Voyage(int id, Trajet t, Date d, int nbPlacesLibres) {
		this.id = id;
		this.t = t;
		this.d = d;
		this.nbPlacesLibres = nbPlacesLibres;
	}

	public void prendreUnSiege() throws Exception {
		if (nbPlacesLibres > 0)
			this.nbPlacesLibres = this.nbPlacesLibres - 1;
		else
			throw new Exception("Plus de places");
	}

	public Date getD() {
		return d;
	}

	public int getId() {
		return id;
	}

	public Trajet getTrajet() {
		return t;
	}

	public int getNbPlacesLibres() {
		return nbPlacesLibres;
	}

	@Override
	public String toString() {
		return t.getSrc() + "->" + t.getDest() + " " + d.toLocaleString() + " " + nbPlacesLibres;
	}
}
