package domain.Vo;

public enum TypeModalite {
	BUS {
		@Override
		public String toString() {
			return "BUS";
		}
	},
	TRAIN {
		@Override
		public String toString() {
			return "TRAIN";
		}
	},
	VOITURE {
		@Override
		public String toString() {
			return "VOITURE";
		}
	}
}
