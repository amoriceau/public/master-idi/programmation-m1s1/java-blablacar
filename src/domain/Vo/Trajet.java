package domain.Vo;

public class Trajet {
	private String dest;
	private String src;

	public Trajet(String dest, String src) {		
		this.dest = dest;
		this.src = src;
	}

	public String getDest() {
		return dest;
	}

	public String getSrc() {
		return src;
	}

	@Override
	public String toString() {
		return "Trajet [dest=" + dest + ", src=" + src + "]";
	}
	
	
}
