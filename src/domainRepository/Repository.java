package domainRepository;

import domain.Aggregate.ReservationVoyagesPourUneModalite;

public interface Repository {
	
	public ReservationVoyagesPourUneModalite findById(int id);
	
	public void save(ReservationVoyagesPourUneModalite voyage);
	
	
}
