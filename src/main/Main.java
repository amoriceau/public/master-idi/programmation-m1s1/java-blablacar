package main;


import java.util.Date;
import java.util.List;

import application.ReservationVoyagesPourUneModaliteServices;
import domain.Vo.Trajet;
import domain.Vo.TypeModalite;

public class Main {

	public static void main(String[] args)  {
		
		//Creation appli
		ReservationVoyagesPourUneModaliteServices application = new ReservationVoyagesPourUneModaliteServices();
		
		
		//Creer les VO
		Trajet t1 = new Trajet("Paris", "Bordeaux");
		Trajet t2 = new Trajet("Rennes", "Bordeaux");
		Trajet t3 = new Trajet("Lyon", "Bordeaux");
		
		
		//Appeler des services de appli pour peupler les données de l'aggregat
		//Creation d'aggregats pour chaque modalite
		int idAggBus = application.creerAggregate(TypeModalite.BUS);
		int idAggTrain = application.creerAggregate(TypeModalite.TRAIN);
		int idAggVoiture = application.creerAggregate(TypeModalite.VOITURE);
		
		//Ajouter un trajet à l'aggregat bus
		application.addTrajet(idAggBus,t1);
		application.addTrajet(idAggBus,t2);
		
		//Ajouter un trajet à l'aggregat train
		application.addTrajet(idAggTrain,t1);
		application.addTrajet(idAggTrain,t2);
		application.addTrajet(idAggTrain,t3);
		
		//Ajouter un trajet à l'aggregat voiture
		application.addTrajet(idAggVoiture,t2);
		application.addTrajet(idAggVoiture,t3);
		
		
		int id=application.addVoyage(idAggBus,t1,new Date(2021, 12, 22),12);
		System.out.println(application.getAsStringVoyage(idAggBus,id));
		
		int id2=application.addVoyage(idAggTrain,t2,new Date(2021, 12, 22),12);
		System.out.println(application.getAsStringVoyage(idAggTrain,id2));
		
		int id3=application.addVoyage(idAggVoiture,t3,new Date(2021, 12, 22),12);
		int id4=application.addVoyage(idAggVoiture,t3,new Date(2020, 12, 22),10);
		System.out.println(application.getAsStringVoyage(idAggVoiture,id3));
		
		
		//Appeler des services de appli pour requêter les données des bus
		for (Trajet t : application.getListTrajets(idAggBus)) {
			System.out.println(t);
		}
		
		//Appeler des services de appli pour requêter les données
		for (Trajet t : application.getListTrajets(idAggTrain)) {
			System.out.println(t);
		}
		
		//Appeler des services de appli pour requêter les données
		for (Trajet t : application.getListTrajets(idAggVoiture)) {
			System.out.println(t);
		}
		List<Integer> trajetBus = application.getListVoyages(idAggBus);
		for (Integer idbus : trajetBus) {
			System.out.println(application.getAsStringVoyage(idAggBus, idbus));
		}
		List<Integer> trajetTrain =application.getListVoyages(idAggTrain);
		for (Integer idtrain : trajetTrain) {
			System.out.println(application.getAsStringVoyage(idAggTrain, idtrain));
		}
		List<Integer> trajetVoiture =application.getListVoyages(idAggVoiture);
		for (Integer idvoiture : trajetVoiture) {
			System.out.println(application.getAsStringVoyage(idAggVoiture, idvoiture));
		}
		
		trajetVoiture =application.getListVoyages(idAggVoiture, new Date(2021,12,01), new Date(2022,01,01));
		for (Integer idvoiture : trajetVoiture) {
			System.out.println(application.getAsStringVoyage(idAggVoiture, idvoiture));
		}
		application.reserverUneplaceDansVoyage(idAggBus,trajetBus.get(0));
		System.out.println(application.getAsStringVoyage(idAggBus,id));

	}

}
